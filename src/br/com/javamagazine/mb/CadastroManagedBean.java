package br.com.javamagazine.mb;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.javamagazine.ejb.CadastroEJB;
import br.com.javamagazine.pojo.Livro;

@SessionScoped
@ManagedBean(name = "cadastro")
public class CadastroManagedBean {

	@EJB
	private CadastroEJB cadastro;

	private Livro livro;

	public CadastroManagedBean() {
		this.livro = new Livro();
	}

	public String gravar() {
		cadastro.salvar(livro);
		return "/index.xhtml?faces-redirect=true";
	}

	public String editar() {
		Map<String, String> params = getParams();
		String id = params.get("id");
		if (id != null){
			this.livro = cadastro.buscarPorId(Long.parseLong(id));
		}
		return "/protegida/cadastro-jsf.xhtml?faces-redirect=true";
	}
	
	public String confirmar() {
		Map<String, String> params = getParams();
		String id = params.get("id");
		if (id != null){
			this.livro = cadastro.buscarPorId(Long.parseLong(id));
		}
		return "/protegida/admin/confirmar.xhtml?faces-redirect=true";
	}
	
	public String excluir() {
		cadastro.excluir(livro.getId());
		return "/protegida/admin/admin.xhtml?faces-redirect=true";
	}

	private Map<String, String> getParams() {
		FacesContext fc = FacesContext.getCurrentInstance();
		return fc.getExternalContext().getRequestParameterMap();
	}

	public List<Livro> getLivros() {
		return cadastro.listar();
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	
	public void logout() throws IOException{
		ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) ctx.getSession(false);
		session.invalidate();
		ctx.redirect(ctx.getRequestContextPath() + "/index.xhtml");
	}
}